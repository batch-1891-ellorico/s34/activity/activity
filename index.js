const express = require('express')
const app = express()
const port = 4000
let users = 
	{
		"username" : 'johndoe', 
		"password" : 'johndoe123'
	}


app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.get('/home', (request, response) => {
	response.send('You are at the home page.')
})

app.get('/users', (request, response) => {
	response.send(users)
})


app.delete('/delete-user', (request, response) => {
	delete users.password
	response.send(users)
})

app.listen(port, () => console.log(`Server is running at port ${port}`))


